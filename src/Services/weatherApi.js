import axios from "axios";

export const weatherApi = (name) => {
  return axios.get(`${process.env.REACT_APP_API_URL_WEATHER}${name}`);
};
