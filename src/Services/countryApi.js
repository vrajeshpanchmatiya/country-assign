import axios from "axios";

export const countryApi = (name) => {
  return axios.get(`${process.env.REACT_APP_API_URL_COUNTRY}${name}`);
};
