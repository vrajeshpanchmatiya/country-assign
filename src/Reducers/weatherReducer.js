import { weatherType } from "../Actions/Type/weatherType";
const initialState = {
  data: [],
};
export const weatherReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case weatherType:
      return {
        ...state,
        data: payload,
      };
    default:
      return state;
  }
};
