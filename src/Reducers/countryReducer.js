import { countryType } from "../Actions/Type/countryType";
const initialState = {
  data: [],
};
export const countryReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case countryType:
      return {
        ...state,
        data: payload,
      };
    default:
      return state;
  }
};
