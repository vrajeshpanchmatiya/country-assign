import { Avatar, Box, Button, Typography } from "@material-ui/core";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { weatherAction } from "../Actions/weatherAction";
import "../Common.scss";
const CountryDetail = () => {
  const dispatch = useDispatch();
  // useSelector for Country Info
  const info = useSelector((state) => {
    return state.country.data;
  });
  // onClick Check Weather dispatch
  const weather = () => {
    dispatch(weatherAction(info.capital));
  };
  // useSelector for Weather info
  const detail = useSelector((state) => {
    return state.weather.data;
  });
  // Detail of Country and Weather
  return (
    <div className="div-screen">
      <Box className="box-container">
        <Box className="small-container-first">
          <Typography>
            <b>Capital: </b>
            {info.capital}
          </Typography>
          <Typography>
            <b>Population: </b>
            {info.population}
          </Typography>
          {info?.latlng && Array.isArray(info.latlng)
            ? info.latlng.map((latlng) => {
                return (
                  <Typography>
                    <b>Latlng</b>
                    {latlng}
                  </Typography>
                );
              })
            : null}
          <Avatar src={info.flag} />
          <Button
            type="submit"
            onClick={weather}
            variant="outlined"
            color="primary"
          >
            Check Weather
          </Button>
        </Box>
        <Box className="small-container-second">
          <Typography>
            <b>Temperature: </b>
            {detail.temperature}
          </Typography>
          <Typography>
            <b>Wind Speed: </b>
            {detail.wind_speed}
          </Typography>
          <Typography>
            <b>Precip: </b>
            {detail.precip}
          </Typography>
          {detail?.weather_icons && Array.isArray(detail.weather_icons)
            ? detail.weather_icons.map((icons) => {
                return <Avatar src={icons} />;
              })
            : null}
        </Box>
      </Box>
    </div>
  );
};
export default CountryDetail;
