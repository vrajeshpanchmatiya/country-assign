import { Box, Button, TextField } from "@material-ui/core";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { countryNameAction } from "../Actions/countryNameAction";
import "../Common.scss";
const CountryForm = () => {
  const [name, setName] = useState(null);
  const dispatch = useDispatch();
  // onChange event for country Name
  const changeName = (e) => {
    setName(e.target.value);
  };
  // onClick event for countryname
  const handleCountryName = () => {
    dispatch(countryNameAction(name));
  };
  //Form of Country
  return (
    <div className="div-screen">
      <Box className="container">
        <TextField
          name={name}
          label="Country Name"
          onChange={changeName}
          variant="outlined"
          color="primary"
        />
        <Link to={{ pathname: "/CountryDetail" }}>
          <Button
            type="submit"
            onClick={handleCountryName}
            variant="outlined"
            color="primary"
          >
            Submit
          </Button>
        </Link>
      </Box>
    </div>
  );
};
export default CountryForm;
