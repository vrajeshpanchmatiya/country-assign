import { countryType } from "./Type/countryType";
import { countryApi } from "../Services/countryApi";
export const countryNameAction = (name) => {
  return async (dispatch) => {
    const detail = await countryApi(name);
    dispatch({ type: countryType, payload: detail.data[0] });
  };
};
