import { weatherType } from "./Type/weatherType";
import { weatherApi } from "../Services/weatherApi";
export const weatherAction = (name) => {
  return async (dispatch) => {
    const details = await weatherApi(name);
    dispatch({ type: weatherType, payload: details.data.current });
  };
};
